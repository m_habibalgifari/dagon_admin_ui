'use strict';

/**
 * @ngdoc overview
 * @name dagonAdminUiApp
 * @description
 * # dagonAdminUiApp
 *
 * Main module of the application.
 */
var dagon = angular
  .module('dagonAdminUiApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'ui.bootstrap',
    'hSweetAlert'
  ])
  .config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/404-http-not-found');
    $stateProvider
		.state('/', {
			url: '',
			templateUrl: 'views/home.html',
			controller: 'HomeCtrl',
			controllerAs: 'home'
		})
		.state('/home', {
			url: '/home',
			templateUrl: 'views/home.html',
			controller: 'HomeCtrl',
			controllerAs: 'home'
		})
		.state('/register', {
			url: '/register',
			templateUrl: 'views/register.html',
			controller: 'RegisterCtrl',
			controllerAs: 'register'
		})
		.state('/register/checkemail', {
			url: '/register/check-email',
			templateUrl: 'views/checkEmail.html',
			controller: 'RegisterCtrl',
			controllerAs: 'register'
		})
		.state('register/emailconfirmation', {
			url: '/register/email-confirmation/:confirmation_id',
			templateUrl: 'views/emailConfirmed.html',
			controller: 'ConfirmationEmailCtrl',
			controllerAs: 'confirmationemail'
		})
		.state('/login', {
			url: '/login',
			templateUrl: 'views/login.html',
			controller: 'LoginCtrl',
			controllerAs: 'login'
		})
		.state('/forgetpassword', {
			url: '/forget-password',
			templateUrl: 'views/forgetPassword.html',
			controller: 'ForgetPasswordCtrl',
			controllerAs: 'forgetpwd'
		})
		.state('/usermanage', {
			url: '/user-manage',
			templateUrl: 'views/userManage.html',
			controller: 'UserManageCtrl',
			controllerAs: 'usermanage'
		})
		.state('/userdetails', {
			url: '/user-details/:UID',
			templateUrl: 'views/userDetails.html',
			controller: 'UserManageCtrl',
			controllerAs: 'usermanage'
		})
		.state('/500InternalServerError', {
			url: '/500-http-internal-server-error',
			templateUrl: 'views/500InternalServerError.html'
		})
		.state('/404NotFound', {
			url: '/404-http-not-found',
			templateUrl: 'views/404NotFound.html'
		})
		.state('/403Forbidden', {
			url: '/403-http-forbidden',
			templateUrl: 'views/403Forbidden.html'
		})
		.state('/401Unautorized', {
			url: '/401-http-unautorized',
			templateUrl: 'views/401Unautorized.html'
		})
		.state('/400BadRequest', {
			url: '/400-http-bad-request',
			templateUrl: 'views/400BadRequest.html'
		});
  })
  
.run(function($rootScope, $cookies){
	
	$rootScope.is_loading = {status : true, message: 'Loading ...'};
	
	window.fbAsyncInit = function() {
		FB.init({
			appId      : '611779068985048',
			channelUrl : '/views/channel.html',
			status	   : true,
			cookie     : true,
			xfbml      : true,
			version    : 'v2.5'
		});
	};

	(function(d){
		var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement('script'); js.id = id; js.async = true;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		ref.parentNode.insertBefore(js, ref);
	}(document));
	
	$rootScope.user_name = $cookies.get('user_name');
	$rootScope.is_loading.status = false;
	$rootScope.is_login = $cookies.get('is_login');
	
	function get_user_style(){
		if($rootScope.is_login){
			return $cookies.get('user_theme');
		}else{
			return 0;
		};
	};
	
	$rootScope.style_pick = get_user_style();
	
	$rootScope.dagon_google_developer_id = '830782888778-m9b8ie04etppqcdt94ntv6ssfl5qfaob.apps.googleusercontent.com';
});
