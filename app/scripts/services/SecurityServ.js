dagon.service('Security', function($http, $cookies, $location, $rootScope, sweet) {
	
	var back_end_host = "http://104.155.205.110:8080";
	var front_end_host = "http://localhost:9000";
	
	var request_data = null;
	
    this.set_request_data = function (data) {
		request_data = data;
		if($cookies.get('session_id')){
			request_data.session_id = $cookies.get('session_id');	
		};
		console.log(JSON.stringify(request_data));
    };
    
    this.do_login = function(){
		
		$rootScope.is_loading.message = 'Logging in...';
		$rootScope.is_loading.status = true;
		
		$http({
			url: back_end_host + '/login', 
			method: "POST",
			params: request_data
		})
		.then(function (response) {
			if(response.message.split(',')[0] == 'success'){
				$cookies.put('is_login', true);
				window.location = front_end_host;
				$rootScope.is_loading.status = false;
			}else{
				$cookies.remove('is_login');
				if(response.data.message.split(',')[1] == 'http'){
					window.location = front_end_host + "/#/"+response.data.message.split(',')[2].trim().replace(' ', '-');
					$rootScope.is_loading.status = false;
				}else{
					sweet.show(response.data.message.split(',')[0].toUpperCase() + ' !', response.data.message.split(',')[1],'error');
					$rootScope.is_loading.status = false;
				}
			}
		},
		function (response){
			window.location = front_end_host + "/#/500-http-internal-server-error";
			$rootScope.is_loading.status = false;
		});
	};
	
    this.do_register = function(){
		
		$rootScope.is_loading.message = 'Registering ...';
		$rootScope.is_loading.status = true;
		
		$http({
			url: back_end_host + '/register', 
			method: "POST",
			params: request_data
		})
		.then(function (response) {
			
			
			
			if(response.data.message.split(',')[0] == 'success'){
				$cookies.put('user_email_address', request_data.user_email);
				window.location = front_end_host + "/#/register/check-email";
				$rootScope.is_loading.status = false;
			}else{
				
				if(response.data.message.split(',')[1] == 'http'){
					window.location = front_end_host + "/#/"+response.data.message.split(',')[2].trim().replace(' ', '-');
					$rootScope.is_loading.status = false;
				}else{
					sweet.show(response.data.message.split(',')[0].toUpperCase() + ' !', response.data.message.split(',')[1],'error');
					$rootScope.is_loading.status = false;
				}
				
			}
		},
		function (response){
			window.location = front_end_host + "/#/500-http-internal-server-error";
			$rootScope.is_loading.status = false;
		});
	};
	
	this.do_confirmation_email = function(register_id){
		
		$rootScope.is_loading.message = 'Confirmation Email ...';
		$rootScope.is_loading.status = true;
		
		$http({
			url: back_end_host + '/register/email-confirmation', 
			method: "POST",
			params: {'register_id' : register_id}
		})
		.then(function (response) {
			if(response.message.split(',')[0] == 'success'){
				$cookies.remove('user_email_address');
				$rootScope.is_loading.status = false;
				return {status: true};
			}else{
				$rootScope.is_loading.status = false;
				return {status: false, message : response.message.split(',')[1].trim()};
			}
		},
		function (response){
			window.location = front_end_host + "/#/500-http-internal-server-error";
			$rootScope.is_loading.status = false;
		});
	};
	
	
	this.check_session_id = function(){
		
		$rootScope.is_loading.message = 'Checking Autorization ...';
		$rootScope.is_loading.status = true;
		
		var session_id = $cookies.get('session_id');

		$http({
			url: back_end_host + '/home', 
			method: "GET",
			params: {'session_id' : session_id}
		})
		.then(function (response) {
			var message = response.data.message.split(',');
			if(message[0] == 'success'){
				$cookies.put('session_id', response.data.session_id);
				$rootScope.is_loading.status = false;				
			}else{
				
				if(message[2].trim().split(' ')[0] == '401'){
					clear_cookies();
				}
				
				if($location.path() == null || $location.path() == '/home'){
					$rootScope.is_loading.status = false;
					get_session_id();
				}else{
					if(session_id){
						$cookies.remove('session_id');
					};
					if(response.data.message.split(',')[1] == 'http'){
						window.location = front_end_host + "/#/"+response.data.message.split(',')[2].trim().replace(' ', '-');
						$rootScope.is_loading.status = false;
					}else{
						sweet.show(response.data.message.split(',')[0].toUpperCase() + ' !', response.data.message.split(',')[1],'error');
						$rootScope.is_loading.status = false;
					}
				};				
			};
		}, 
		function (response){
			window.location = front_end_host + "/#/500-http-internal-server-error";
			$rootScope.is_loading.status = false;
		});
	};
	
	function get_session_id(){
		
		$rootScope.is_loading.message = 'Getting Autorization ...';
		$rootScope.is_loading.status = true;
		
		$http({
			url: back_end_host + '/home', 
			method: "GET"
		})
		.then(function (response) {
			$cookies.put('session_id', response.data.session_id);
			$rootScope.is_loading.status = false;
		},
		function (response){
			window.location = front_end_host + "/#/500-http-internal-server-error";
			$rootScope.is_loading.status = false;
		});
	};
	
	function clear_cookies(){
		var cookies = $cookies.getAll();
		angular.forEach(cookies, function(value, key) {
			$cookies.remove(key);
		});
	};
	
	
	
	
	
	
	
	
	
	
	
	
	
	this.facebook_service = function(type){	
			
		rootScope = $rootScope;
		facebook_set_request_data = this.set_request_data;
		facebook_do_dagon_login = this.do_login;
		facebook_do_dagon_register = this.do_register;
		facebook_action_type = type;
		
		facebook_check_user_autorization();
	};
	
	function facebook_check_user_autorization(){
		rootScope.is_loading.status = true;
		rootScope.is_loading.message = 'Checking facebook user autorization ...';
		
		FB.getLoginStatus(function(response) {
			if (response.status === 'connected') {
				facebook_get_user_data();
			} else {
				facebook_get_user_autorize();
			}
		});
		
	};
	
	function facebook_get_user_autorize(){
		rootScope.is_loading.message = 'Getting facebook user autorization ...';
		
		FB.login(function(response) {
			if (response.authResponse) {
				facebook_get_user_data();

			} else {
				sweet.show('ERROR !', 'Failed getting user autorization!','error');
				rootScope.is_loading.status = false;
				facebook_remove_root_variable(); 
			}
		}, {scope : 'email'});
	};
	
	function facebook_get_user_data(){
		rootScope.is_loading.message = 'Getting facebook user info ...';
		
		FB.api('/me', {fields: 'id,name,email'}, function(response) {
			request_data = {};
			request_data.user_real_name = response.name;
			request_data.user_email = response.email;
			request_data.user_password = response.id;
			request_data.user_password_retype = response.id;
			request_data.user_login_type = 'facebook';
			facebook_do_action();
		});
	};
	
	function facebook_do_action(){
		facebook_set_request_data(request_data);
		switch(facebook_action_type) {
			case 'login':
				facebook_do_dagon_login();
			break;
			case 'register':
				facebook_do_dagon_register();
				facebook_do_dagon_login();
			break;
			default:
				
		}
		facebook_remove_root_variable(); 
	};
	
	function facebook_remove_root_variable(){
		rootScope = undefined;
		facebook_set_request_data = undefined;
		facebook_do_dagon_login = undefined;
		facebook_do_dagon_register = undefined;
		facebook_action_type = undefined;
	};
	
});
