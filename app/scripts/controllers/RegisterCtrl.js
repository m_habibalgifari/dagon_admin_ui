'use strict';

/**
 * @ngdoc function
 * @name dagonAdminUiApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the dagonAdminUiApp
 */
dagon
  .controller('RegisterCtrl', function ($scope, Security, $cookies) {
	
	$scope.user_email = {};
	$scope.user_email.address = $cookies.get('user_email_address');
	$scope.user_email.link = 'http://' + $cookies.get('user_email_address').split('@')[1];
	
	var register_id = null;
	
	//Security.check_session_id();
	
	$scope.request_data = {};
	
    var form_height = $('#dagon-registration-container').parent().height();
    var button_space_height = (form_height/2)-60;
    $scope.button_space = {height : button_space_height};
    
    var do_registration = function(request_data){
		request_data.user_login_type = 'dagon';
		Security.set_request_data(request_data);
		Security.do_register();
	};
	
	
	$scope.do_validation_params_and_registration = function(request_data){
		$scope.error_message = {};
		if(!request_data.user_real_name || request_data.user_real_name.length<4 || request_data.user_real_name.length>30){
			$scope.error_message.user_real_name = 'Full name length must be between 4-30 character.';
		};
		
		if(!request_data.user_email || request_data.user_email.length<4 || request_data.user_email.length>30){
			$scope.error_message.user_email = 'Email address length must be between 4-30 character and have to contain "@" character.';
		};
		
		if(!request_data.user_password || request_data.user_password.length<4 || request_data.user_password.length>30){
			$scope.error_message.user_password = 'Password length must be between 4-30 character.';
		};
		
		if(request_data.user_password != request_data.user_password_retype){
			$scope.error_message.user_password_retype = 'Password does not same.';
		};
		
		if(!$scope.error_message.user_real_name && !$scope.error_message.user_email && !$scope.error_message.user_password && !$scope.error_message.user_password_retype){
			do_registration(request_data);
		};
	};
	
	$scope.do_registration_facebook = function(){
		Security.facebook_service('register');
	};
	
	$scope.do_registration_google_plus = function(){
		var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });

	};
  });
