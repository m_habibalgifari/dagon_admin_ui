'use strict';

/**
 * @ngdoc function
 * @name dagonAdminUiApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the dagonAdminUiApp
 */
dagon
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
