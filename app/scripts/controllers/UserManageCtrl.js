'use strict';

/**
 * @ngdoc function
 * @name dagonAdminUiApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the dagonAdminUiApp
 */
dagon
  .controller('UserManageCtrl', function ($scope, Security) {
	Security.check_session_id(); 
	$scope.user_details = {};
	$scope.user_details.status = 'Active';
	$scope.status_list = ['Active', 'Inactive'];
  });
