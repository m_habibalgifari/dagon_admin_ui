'use strict';

/**
 * @ngdoc function
 * @name dagonAdminUiApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the dagonAdminUiApp
 */
dagon
  .controller('LoginCtrl', function ($scope, Security, $cookies) {
	
	//Security.check_session_id();
	
	$scope.do_login = function(request_data){
		$scope.toggle_login_popover();
		request_data.user_login_type = 'dagon';
		Security.set_request_data(request_data);
		Security.do_login();
		
	};
	
	$scope.do_facebook_login = function(){
		$scope.toggle_login_popover();
		Security.facebook_service('login');
	};
	
	$scope.do_logout = function(){
		Security.clear_cookies();
		window.location = "http://localhost:9000";
	};

  });
