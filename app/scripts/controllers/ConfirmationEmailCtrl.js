'use strict';

/**
 * @ngdoc function
 * @name dagonAdminUiApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the dagonAdminUiApp
 */
dagon
  .controller('ConfirmationEmailCtrl', function ($scope, Security, $location) {
	
	var register_id = $location.url().split('/')[3];
	var is_success = Security.do_confirmation_email(register_id);
	
	$scope.confirmation_status = is_success;
    
  });
