'use strict';

/**
 * @ngdoc function
 * @name dagonAdminUiApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the dagonAdminUiApp
 */
dagon
  .controller('IndexCtrl', function ($scope, Security, $cookies, $rootScope) {
	
	$scope.cok = $cookies.getAll();
	
	$scope.set_style = function(index){
		 
		 $rootScope.style_pick = index;
	};
	
	$scope.toggle_login_popover = function(){
		$scope.login_popover_open = !$scope.login_popover_open;
	};
	
    $scope.styles = [
		{
			'background-color' : '#0288D1',
			'color' : 'white'
		},
		{
			"background-color" : "#01579b",
			"color" : "white"
		},
		{
			"background-color" : "#F44336",
			"color" : "white"
		},
		{
			"background-color" : "#3F51B5",
			"color" : "white"
		},
		{
			"background-color" : "#00BCD4",
			"color" : "white"
		},
		{
			"background-color" : "#009688",
			"color" : "white"
		},
		{
			"background-color" : "#4CAF50",
			"color" : "white"
		},
		{
			"background-color" : "#CDDC39",
			"color" : "white"
		},
		{
			"background-color" : "#FF5722",
			"color" : "white"
		},
		{
			"background-color" : "#9E9E9E",
			"color" : "white"
		},
		{
			"background-color" : "#607D8B",
			"color" : "white"
		},
		{
			"background-color" : "#795548",
			"color" : "white"
		}
	];
	
	$scope.page_state = "home";
	
	$scope.active_menu = function(menu){
		if(menu == $scope.page_state){
			return 'dagon-menu-actived';
		};
	};
	
	$scope.theme_popup = {url : "/views/themePopupTemplate.html"}; 
	$scope.menu_popup = {url : "/views/menuPopupTemplate.html"}; 
	$scope.login_popup = {url : "/views/loginPopupTemplate.html"}; 
  });
