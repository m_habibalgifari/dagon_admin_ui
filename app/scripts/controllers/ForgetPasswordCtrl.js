'use strict';

/**
 * @ngdoc function
 * @name dagonAdminUiApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the dagonAdminUiApp
 */
dagon
  .controller('ForgetPasswordCtrl', function ($scope, Security) {
    Security.check_session_id();
  });
